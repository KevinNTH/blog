/**
 * HomepageControllerController
 *
 * @description :: Server-side logic for managing Homepagecontrollers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `HomepageControllerController.index()`
   */
  index: function(req, res) {
    User.find().exec((err, records) => {
      return res.view('homepage/home', {
				users: records
      });
    });
  },

  redir: function(req, res) {
    res.redirect('/');
  },

  protected: function(req, res) {
    res.send('coucou');
  }
};
