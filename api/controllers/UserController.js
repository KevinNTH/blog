/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // /**
  //  * UserController.create()
  //  */
  // create: function(req, res) {
  //   User.create(req.body).exec((data) => {
  //     req.body.newUserSignedUp = true;
  //     return res.created(data, {
  //       view: 'homepage/home'
  //     });
  //   });
  // }
};
